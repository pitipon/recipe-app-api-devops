#!/bin/sh

set -e

aws-vault exec pitipon.pitipon --duration=12h

docker-compose -f deploy/docker-compose.yml tun --rm terraform init